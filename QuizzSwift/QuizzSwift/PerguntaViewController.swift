//
//  PerguntaViewController.swift
//  QuizzSwift
//
//  Created by Michella Aguiar Coelho on 5/7/15.
//  Copyright (c) 2015 Michella Aguiar Coelho. All rights reserved.
//

import UIKit

class PerguntaViewController: UIViewController {

    @IBOutlet weak var resp1: UIButton!
    @IBOutlet weak var perguntaLabel: UILabel!
    
    @IBOutlet weak var resp2: UIButton!
    
    @IBOutlet weak var resp3: UIButton!
    
    @IBOutlet weak var resp4: UIButton!
    
    @IBOutlet weak var pontosLabel: UILabel!
    
    
    @IBOutlet weak var tempoRestanteLabel: UILabel!
    
    var perguntasDB: [Pergunta] = [Pergunta]()
    
    var nomeDoArquivo: (String) = "perguntasDivertidas.txt"
    
    var perguntaDaVez: (Pergunta)!
    
    var tempo:(Int) = 0
    
    var vez:(Int) = 0
    
    var timer: (NSTimer)!
    var timerToDisplay = NSTimer()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        
        
        
        self.setarNomeDoArquivo()
        self.populaDados()
        self.setarMinimoAcertos()
        
        vez = 0
        tempo = 5
        
        timerToDisplay = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: Selector ("atualizaLabelTempo"), userInfo: nil, repeats: true)
        
        self.atualizar()
        
    }
    
    func atualizaLabelAcertos () {
        
        self.pontosLabel.text =  String(QuizzDataManager.sharedInstance.pontos) as String
    }
    
    func setarMinimoAcertos () {
        var minimo = Int (perguntasDB.count/3)
         QuizzDataManager.sharedInstance.minimoAcertos = minimo
        
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    
    func setarNomeDoArquivo () {
        if (QuizzDataManager.sharedInstance.tipoDoQuizz == 0){
            nomeDoArquivo = "perguntasDivertidas.txt"
        } else if (QuizzDataManager.sharedInstance.tipoDoQuizz == 1) {
            nomeDoArquivo = "perguntasCulturais.txt"

        } else if (QuizzDataManager.sharedInstance.tipoDoQuizz == 2) {
            nomeDoArquivo = "perguntasConhecimentos.txt"

        }
        
    }
    
    
    func LoadStringFromFile( file : NSString) -> NSString
    {
        var documentPath : NSString = NSBundle.mainBundle().bundlePath.stringByAppendingPathComponent(file as String);

        var contentOfFile = NSString(contentsOfFile: documentPath as String, encoding: NSUTF8StringEncoding, error: nil);
println("\(documentPath)")
        println( contentOfFile )

        return contentOfFile!;
        
    }
    
    
    func populaDados () {
        

        var dados = self.LoadStringFromFile(nomeDoArquivo) as String
        //NSLog("%s",dados)
        var perguntas = dados.componentsSeparatedByString("*")
        for (var i=0; i<perguntas.count; i++) {
            var pergunta = Pergunta()

            var campos: [String] = perguntas[i].componentsSeparatedByString("-")
            //var pergunta = Pergunta()
            var teste:String = campos[0]
            pergunta.textoPergunta = campos[0]
            pergunta.respostasPergunta = [campos[1],campos[2],campos[3],campos[4]]
            pergunta.indiceRespostaCerta = campos[5].toInt()!
            
            perguntasDB.append(pergunta)
            //perguntasDB.insert(pergunta, atIndex: perguntasDB.count)
            //NSLog("%s",pergunta.textoPergunta)
        }
        
        
    }
    
    func randInRange(range: Range<Int>) -> Int {
        // arc4random_uniform(_: UInt32) returns UInt32, so it needs explicit type conversion to Int
        // note that the random number is unsigned so we don't have to worry that the modulo
        // operation can have a negative output
        return  Int(arc4random_uniform(UInt32(range.endIndex - range.startIndex))) + range.startIndex
    }
    
    func sorteioPerguntaDaVez() {
        //perguntaDaVez = Pergunta()
        println("Iniciou o sorteio")
        var tamanho = perguntasDB.count-1;
        var randomico = Int(randInRange(0...tamanho))
        //arc4random_uniform ( <#UInt32#> (tamanho) )
        
        //self.randInRange(0...tamanho)
        println("posicao random \(randomico)")
        println("\(tamanho)")
        if ( randomico >= 0 && perguntasDB.count > 0 && randomico < perguntasDB.count) {
            perguntaDaVez = perguntasDB[randomico]
            println("\(perguntasDB[randomico].textoPergunta)")

            perguntasDB.removeAtIndex(randomico)
            println("entrou")
            println("\(perguntaDaVez.textoPergunta)")
            
        }else{
            perguntaDaVez = perguntasDB[0]
            
        }
        
        
        for (var i=0; i < perguntasDB.count; i++) {
            println("pergunta posicao \(i) texto: \(perguntasDB[i].textoPergunta)")
        }
        
        
    }
    
    func atualizaLabelTempo () {
        if(tempo>0) {
            tempo--
        }else{
            vez++
            self.atualizar()
            println("atualizou")
        }
        
        self.tempoRestanteLabel.text = "\(tempo)"

    }
    
    func atualizar () {
        tempo = 11
        if (vez == -1) {
            vez = 0
        }
        
        var tam:(Int) = perguntasDB.count
        var partInteiraTam = Int (tam-(tam/5))
        if (vez >= partInteiraTam) {
            
            //timer.invalidate()
            //timer = nil
            timerToDisplay.invalidate()
            //timerToDisplay = nil
            
            //chamar a viewController
            
            let storyBoard: (UIStoryboard) = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewControllerWithIdentifier("Placar") as! UIViewController
            
            self.presentViewController(vc, animated: true, completion: nil)
        
        } else {
            self.sorteioPerguntaDaVez()
            
            self.perguntaLabel.text = perguntaDaVez.textoPergunta
            self.resp1.setTitle(perguntaDaVez.respostasPergunta[0], forState: .Normal)
            self.resp2.setTitle(perguntaDaVez.respostasPergunta[1], forState: .Normal)
            self.resp3.setTitle(perguntaDaVez.respostasPergunta[2], forState: .Normal)
            self.resp4.setTitle(perguntaDaVez.respostasPergunta[3], forState: .Normal)
            
        }

        
    }
    
    func randomNumber(range: Range<Int> = 1...6) -> Int {
        let min = range.startIndex
        let max = range.endIndex
        return Int(arc4random_uniform(UInt32(max - min))) + min
    }
    
    @IBAction func resp4Pressed(sender: AnyObject) {
        
        if (perguntaDaVez.indiceRespostaCerta==3) {
            QuizzDataManager.sharedInstance.pontos = QuizzDataManager.sharedInstance.pontos+1
            
            
        }
        vez++;
        self.pontosLabel.text = "\(QuizzDataManager.sharedInstance.pontos)"
       
        
        self.atualizar()
        
    }
    @IBAction func resp3Pressed(sender: AnyObject) {
        if (perguntaDaVez.indiceRespostaCerta==2) {
            QuizzDataManager.sharedInstance.pontos = QuizzDataManager.sharedInstance.pontos+1
            
            
        }
        vez++;
        self.pontosLabel.text = "\(QuizzDataManager.sharedInstance.pontos)"
        
        
        self.atualizar()
        
        
    }

    @IBAction func resp2Pressed(sender: AnyObject) {
        if (perguntaDaVez.indiceRespostaCerta==1) {
            QuizzDataManager.sharedInstance.pontos = QuizzDataManager.sharedInstance.pontos+1
            
            
        }
        vez++;
        self.pontosLabel.text = "\(QuizzDataManager.sharedInstance.pontos)"
        
        
        self.atualizar()
    }
    
    
    
    
    @IBAction func resp1Pressed(sender: AnyObject) {
        if (perguntaDaVez.indiceRespostaCerta==0) {
            QuizzDataManager.sharedInstance.pontos = QuizzDataManager.sharedInstance.pontos+1
            
            
        }
        vez++;
        self.pontosLabel.text = "\(QuizzDataManager.sharedInstance.pontos)"
        
        
        self.atualizar()
    }
    
}
