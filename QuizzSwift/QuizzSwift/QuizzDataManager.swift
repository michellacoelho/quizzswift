//
//  QuizzDataManager.swift
//  QuizzSwift
//
//  Created by Michella Aguiar Coelho on 5/6/15.
//  Copyright (c) 2015 Michella Aguiar Coelho. All rights reserved.
//

import UIKit
private let _QuizzDataManagerInstance = QuizzDataManager()


class QuizzDataManager: NSObject {
    
    
    //static var instance: FGSingleton
    
    static let sharedInstance = QuizzDataManager()
    
    var pontos=0
    var tipoDoQuizz=0
    var nomeJogador="Desconhecido"
    var minimoAcertos = 0;
    
    
    
    func resetDados() {
        pontos = 0
        tipoDoQuizz = 0
        nomeJogador = "Desconhecido"
        minimoAcertos = 0
        
    }
    
   
}
