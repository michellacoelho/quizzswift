//
//  Pergunta.swift
//  QuizzSwift
//
//  Created by Michella Aguiar Coelho on 5/5/15.
//  Copyright (c) 2015 Michella Aguiar Coelho. All rights reserved.
//

import UIKit

class Pergunta {
    var textoPergunta:String!
   var respostasPergunta: [String]!
    var indiceRespostaCerta:Int = 0
   
    
    init() {
        textoPergunta = ""
        respostasPergunta = ["","","",""]
        indiceRespostaCerta = 0
    }
    
}
