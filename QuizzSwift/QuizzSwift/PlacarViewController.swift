
//
//  PlacarViewController.swift
//  QuizzSwift
//
//  Created by Michella Aguiar Coelho on 5/8/15.
//  Copyright (c) 2015 Michella Aguiar Coelho. All rights reserved.
//

import UIKit

class PlacarViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    
    var scoreDivertido:[ElementoScore] = [ElementoScore]()
    var scoreCultural:[ElementoScore] = [ElementoScore]()
    var scoreConhecimentosGerais:[ElementoScore] = [ElementoScore]()
    var scoreSalvo:String!
    var score:[ElementoScore]  = [ElementoScore] ()
    
    


    
    

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var pontosFinaisLable: UILabel!
    
    @IBOutlet weak var nomeJogadorLabel: UILabel!
    
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var keyToSalvar:(String) = ""
        
        
        if (QuizzDataManager.sharedInstance.tipoDoQuizz==0){
            keyToSalvar = "score0";
        }else if (QuizzDataManager.sharedInstance.tipoDoQuizz==1){
        
            keyToSalvar = "score1";

        }else if (QuizzDataManager.sharedInstance.tipoDoQuizz==2){
            
            keyToSalvar = "score2";

        }

        
        
        scoreDivertido = self.leDados("score0")
        scoreCultural = self.leDados("score1")
        scoreConhecimentosGerais = self.leDados("score2")
        
        score = leDados(keyToSalvar)
        
        if (score.count==0) {
            self.salvaDados()
            score = self.leDados(keyToSalvar)
        } else {
            if (QuizzDataManager.sharedInstance.pontos >= QuizzDataManager.sharedInstance.minimoAcertos){
                self.salvaDados()
                score = self.leDados(keyToSalvar)
            }
            
        }
        
        
        
        
        
        

        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(animated: Bool) {
        self.nomeJogadorLabel.text = QuizzDataManager.sharedInstance.nomeJogador;
        self.pontosFinaisLable.text = "\(QuizzDataManager.sharedInstance.pontos)"
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    func leDados (tipoScore: String) -> [ElementoScore] {
        
        var resultado:[ElementoScore] = [ElementoScore]()
        
        var defaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
        
        
        //var scoreLidoAgora:String!
        var keyToSalvar = tipoScore;
        
        
        var teste: AnyObject? = defaults.objectForKey(keyToSalvar)
        println("leu \(teste)")
        
        //var scoreLidoAgora:String = defaults.objectForKey(keyToSalvar) as! String;
        
       //defaults.removeObjectForKey(keyToSalvar)
        
        if (teste != nil ) {
             var scoreSalvo:String = String (teste as! NSString)
            var manipulaScoreSalvo:[String] = scoreSalvo.componentsSeparatedByString("*")
            for (var i=0; i < manipulaScoreSalvo.count; i++) {
                var campos = manipulaScoreSalvo[i].componentsSeparatedByString("-")
                var elementoScore:ElementoScore = ElementoScore()
                elementoScore.nome =  campos[0]
                //if (campos[1] != nil) {
                elementoScore.pontuacao = campos[1].toInt()!
                //}
                resultado.append(elementoScore)
                //println("AQUIIIIIIII \(resultado[i])")
                
            }
            
        }
        
        
        
        return resultado;

        
    }
    
    func salvaDados () {
        
        var placar:String!
        var keyToSalvar:String!
        if ( QuizzDataManager.sharedInstance.tipoDoQuizz == 0 ) {
            keyToSalvar = "score0"
            
        } else if ( QuizzDataManager.sharedInstance.tipoDoQuizz == 1) {
            keyToSalvar = "score1"
        } else if ( QuizzDataManager.sharedInstance.tipoDoQuizz == 2) {
            keyToSalvar = "score2"
        }
        
        if (score.count == 0) {
            placar = "\(QuizzDataManager.sharedInstance.nomeJogador)-\(QuizzDataManager.sharedInstance.pontos)"
        } else {
            placar = "\(scoreSalvo)*\(QuizzDataManager.sharedInstance.nomeJogador)-\(QuizzDataManager.sharedInstance.pontos)"
        }
        
        var defaults:NSUserDefaults = NSUserDefaults.standardUserDefaults();
        defaults.setObject(placar, forKey: keyToSalvar)
        
        defaults.synchronize()
        scoreDivertido = self.leDados("score0")
        scoreCultural = self.leDados("score1")
        scoreConhecimentosGerais = self.leDados("score2")
        self.tableView.reloadData()
    
        
        
        
        
        
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if (section==0) {
            return "Divertido";
        }else if (section==1) {
            return "Cultural";
        }else{
            return "Conhecimentos Gerais";
        }

    }
    
    
    @IBAction func tryAgainPressed(sender: AnyObject) {
        
        QuizzDataManager.sharedInstance.resetDados()
        
        
        let storyBoard: (UIStoryboard) = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewControllerWithIdentifier("Comeco") as! UIViewController
        
        self.presentViewController(vc, animated: true, completion: nil)
        
        
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
         var  cellIdentifier = "Cell"
        
        
        var cell:UITableViewCell? = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as? UITableViewCell
        if (cell != nil){
            //cell = UITableViewCell(style: UITableViewCellStyle.Subtitle,
               // reuseIdentifier: cellIdentifier)
            
            
        } else {
            cell = UITableViewCell(style: UITableViewCellStyle.Subtitle,
                reuseIdentifier: cellIdentifier)
        }

        var linha,subtitle:String!
        
        
        if (indexPath.section==0) {
            linha = scoreDivertido[indexPath.row].nome
            subtitle = "\(scoreDivertido[indexPath.row].pontuacao)"
            
        }else if (indexPath.section==1){
            linha = scoreCultural[indexPath.row].nome
            subtitle = "\(scoreCultural[indexPath.row].pontuacao)"

        } else{
            linha = scoreConhecimentosGerais[indexPath.row].nome
            subtitle = "\(scoreConhecimentosGerais[indexPath.row].pontuacao)"

        }

        cell?.detailTextLabel?.text = subtitle
        cell?.textLabel?.text = linha
        
        
        return cell!;
        
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (section==0) {
            return scoreDivertido.count;
        } else if (section==1){
            return scoreCultural.count;
        }else{
            return scoreConhecimentosGerais.count;
        }
    }
    
    @IBAction func clearScore(sender: AnyObject) {
        
        var keyToSalvar:String!
        if ( QuizzDataManager.sharedInstance.tipoDoQuizz == 0 ) {
            keyToSalvar = "score0"
            
        } else if ( QuizzDataManager.sharedInstance.tipoDoQuizz == 1) {
            keyToSalvar = "score1"
        } else if ( QuizzDataManager.sharedInstance.tipoDoQuizz == 2) {
            keyToSalvar = "score2"
        }

        
        
        
//        
//        NSString *keyToSalvar;
//        
//        if ([[QuizzDataManager getQuizzDataManager]tipoDoQuizz]==0) {
//            keyToSalvar = @"score0";
//        }else if ([[QuizzDataManager getQuizzDataManager]tipoDoQuizz]==1){
//            keyToSalvar = @"score1";
//        }else{
//            keyToSalvar = @"score2";
//        }
//        
//        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//        [defaults setObject:@"" forKey:keyToSalvar];
//        //[defaults setInteger:9001 forKey:@"HighScore"];
//        [defaults synchronize];
//        
//        [score removeAllObjects];
//        [scoreConhecimentosGerais removeAllObjects];
//        [scoreCultural removeAllObjects];
//        [scoreDivertido removeAllObjects];
//        [_tableView reloadData];
//        
//        if ([_tableView numberOfRowsInSection:0]==0) {
//            [_tableView setHidden:TRUE];
//            [_highScoreLabel setHidden:TRUE];
//        }else{
//            [_tableView setHidden:FALSE];
//            [_highScoreLabel setHidden:FALSE];
//        }
//        
//        
//        scoreDivertido = [self leDados:@"score0"];
//        scoreCultural = [self leDados:@"score1"];
//        scoreConhecimentosGerais = [self leDados:@"score2"];
//        [_tableView reloadData];
//        
//
//        
        
        
        
        
        
        
        
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 3;
    }
}
