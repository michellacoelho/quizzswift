//
//  ViewController.swift
//  QuizzSwift
//
//  Created by Michella Aguiar Coelho on 5/5/15.
//  Copyright (c) 2015 Michella Aguiar Coelho. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource,UITextFieldDelegate{
    @IBOutlet weak var tipoDoQuizzPickerView: UIPickerView!

    @IBOutlet weak var nomeTextField: UITextField!
    var numeroDeClicks = 0
    var dataPicker = ["Divertido", "Cultural","Conhecimentos Gerais"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        return dataPicker[row]
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return dataPicker.count
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        QuizzDataManager.sharedInstance.tipoDoQuizz = row;
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
    
    @IBAction func iniciarJogo(sender: AnyObject) {
        
        if (nomeTextField.text == "") {
            QuizzDataManager.sharedInstance.nomeJogador = "Desconhecido"

        } else {
            QuizzDataManager.sharedInstance.nomeJogador = nomeTextField.text

        }
        
    }
    
    
    
    

}

